package com.besik;

import com.besik.algorithm.Game;
import com.besik.algorithm.GameImpl;
import com.besik.entity.Hand;
import com.besik.entity.Player;
import com.besik.strategy.StaticStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class GameTest {

    private Game game;

    public GameTest(Game game) {
        this.game = game;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters() {
        return Arrays.asList(new Object[][]{
                {new GameImpl()}
        });
    }

    @Test
    public void scissorsBeatsPaper() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.SCISSOR));
        Player player2 = new Player("player2", new StaticStrategy(Hand.PAPER));
        assertEquals(player1, game.getWinner(player1, player2));
    }

    @Test
    public void paperBeatsRock() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.PAPER));
        Player player2 = new Player("player2", new StaticStrategy(Hand.ROCK));
        assertEquals(player1, game.getWinner(player1, player2));
    }

    @Test
    public void rockBeatsScissors() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.ROCK));
        Player player2 = new Player("player2", new StaticStrategy(Hand.SCISSOR));
        assertEquals(player1, game.getWinner(player1, player2));
    }


    @Test
    public void scissorsDrawScissors() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.SCISSOR));
        Player player2 = new Player("player2", new StaticStrategy(Hand.SCISSOR));
        assertEquals(null, game.getWinner(player1, player2));
    }

    @Test
    public void paperDrawPaper() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.PAPER));
        Player player2 = new Player("player2", new StaticStrategy(Hand.PAPER));
        assertEquals(null, game.getWinner(player1, player2));
    }

    @Test
    public void rockDrawRock() {
        Player player1 = new Player("player1", new StaticStrategy(Hand.ROCK));
        Player player2 = new Player("player2", new StaticStrategy(Hand.ROCK));
        assertEquals(null, game.getWinner(player1, player2));
    }
}