package com.besik.strategy;

import com.besik.entity.Hand;

public interface GameStrategy {
    /**
     * @return method return one variant of choose
     */
    Hand throwHand();
}
