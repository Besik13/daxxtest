package com.besik.strategy;

import com.besik.entity.Hand;

public class StaticStrategy implements GameStrategy {
    private Hand hand;

    /**
     * Constructor - creating a new object with default values
     *
     * @param hand - throwing hand
     */
    public StaticStrategy(Hand hand) {
        this.hand = hand;
    }

    public Hand throwHand() {
        return hand;
    }
}
