package com.besik.strategy;

import com.besik.entity.Hand;

import java.util.Random;

public class RandomStrategy implements GameStrategy {
    public Hand throwHand() {
        Random random = new Random();
        Hand[] hands = Hand.values();
        int index = random.nextInt(hands.length);
        return hands[index];
    }
}
