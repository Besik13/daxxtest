package com.besik.entity;

import com.besik.strategy.GameStrategy;

public class Player {
    private String name;
    private GameStrategy strategy;

    public Player(String name, GameStrategy strategy) {
        this.name = name;
        this.strategy = strategy;
    }

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(GameStrategy strategy) {
        this.strategy = strategy;
    }

    public Hand executeStrategy() {
        return strategy.throwHand();
    }

}
