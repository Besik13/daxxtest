package com.besik.entity;

/**
 * Possible player selection options
 */
public enum Hand {
    ROCK, SCISSOR, PAPER
}
