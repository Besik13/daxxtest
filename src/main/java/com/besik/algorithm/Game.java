package com.besik.algorithm;

import com.besik.entity.Player;

public interface Game {
    Player getWinner(Player firstPlayer, Player secondPlayer);
}
