package com.besik.algorithm;

import com.besik.entity.Player;

public class GameImpl implements Game {
    /**
     * Matrix of results.
     * 0 is defeat
     * 1 is victory
     * -1 is draw
     * +-------+-------+-------+-------+
     * |       |ROCK   |SCISSOR|PAPER  |
     * +-------+-------+-------+-------+
     * |ROCK   |-1     |1      |0      |
     * |-------+-------+-------+-------+
     * |SCISSOR|0      |-1     |1      |
     * |-------+-------+-------+-------+
     * |PAPER  |1      |0      |-1     |
     * +-------+-------+-------+-------+
     */
    private int[][] result = {
            {-1, 1, 0},
            {0, -1, 1},
            {1, 0, -1}
    };

    /**
     * @return returns player or NULL if drawn
     */
    public Player getWinner(Player firstPlayer, Player secondPlayer) {
        if (result[firstPlayer.executeStrategy().ordinal()][secondPlayer.executeStrategy().ordinal()] == 1) {
            return firstPlayer;
        }
        if (result[firstPlayer.executeStrategy().ordinal()][secondPlayer.executeStrategy().ordinal()] == 0) {
            return secondPlayer;
        }
        return null;
    }
}
