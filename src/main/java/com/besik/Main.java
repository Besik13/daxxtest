package com.besik;

import com.besik.algorithm.Game;
import com.besik.algorithm.GameImpl;
import com.besik.entity.Hand;
import com.besik.entity.Player;
import com.besik.strategy.RandomStrategy;
import com.besik.strategy.StaticStrategy;

public class Main {

    public static void main(String[] args) {
        Game game = new GameImpl();
        Player player1 = new Player("Player1");
        Player player2 = new Player("Player2");
        player1.setStrategy(new StaticStrategy(Hand.ROCK));
        player2.setStrategy(new RandomStrategy());
        Player winner;
        for (int i = 0; i < 100; i++) {
            winner = game.getWinner(player1, player2);
            if (winner == null) {
                System.out.println("In a draw");
            } else {
                System.out.println(winner.getName() + " won");
            }
        }
    }
}
